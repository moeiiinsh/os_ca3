#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <unistd.h>

using namespace std;

#define NUMBER_OF_CLASSES 4
#define MIN -10000000
#define MAX 10000000

vector< vector<long double> > tokenize(vector<string> data) {
    vector< vector<long double> > result;
    for(int i = 0; i < data.size(); i++) {
        string row = data[i];
        string token;
        stringstream row_stream(row);
        vector<long double> tokenized_row;

        while(getline(row_stream,token, ',')) 
            tokenized_row.push_back(stold(token));

        result.push_back(tokenized_row);
    }
    return result;
}

vector<long double> find_minimums(vector< vector<long double> > train_data) {
    vector<long double> minimums;
    for(int i = 0; i < train_data[0].size(); i++) 
        minimums.push_back(MAX);
    
    for(int i = 0; i < train_data.size(); i++) {
        vector<long double> row = train_data[i];
        for(int j = 0; j < row.size(); j++) {
            if(row[j] < minimums[j])
                minimums[j] = row[j];
        }
    }
    return minimums; 
}

vector<long double> find_maximums(vector< vector<long double> > train_data) {
    vector<long double> maximums;
    for(int i = 0; i < train_data[0].size(); i++) 
        maximums.push_back(MIN);
    
    for(int i = 0; i < train_data.size(); i++) {
        vector<long double> row = train_data[i];
        for(int j = 0; j < row.size(); j++) {
            if(row[j] > maximums[j])
                maximums[j] = row[j];
        }
    }
    return maximums;
}

long double calc_class_score(vector<long double> phone_info, vector<long double> class_weights, vector<long double> maximums, vector<long double> minimums) {
    long double total_score = 0.0;
    for(int i = 0; i < phone_info.size() - 1; i++) {
        long double normalized_value = (phone_info[i] - minimums[i]) / (maximums[i] - minimums[i]);
        total_score += (normalized_value * class_weights[i]);
    }
    long double bias = class_weights[phone_info.size() - 1];
    return total_score + bias;
}


int main(int argc, char **argv) {
    char *data_dir = argv[1];
    
    string dataset_dir(data_dir);
    string weights_dir = dataset_dir + "weights.csv";
    string train_dir = dataset_dir + "train.csv";

    ifstream train_file(train_dir);
    ifstream weights_file(weights_dir);
    
    string phone_info_row, weights_row;
    
    getline(train_file, phone_info_row);
    getline(weights_file, weights_row);

    vector<string> class_weights;
    vector<string> train_data;

    while(getline(weights_file, weights_row)) 
        class_weights.push_back(weights_row);

    while(getline(train_file, phone_info_row)) 
        train_data.push_back(phone_info_row);

    vector< vector<long double> > tokenized_weights = tokenize(class_weights);
    vector< vector<long double> > tokenized_train_data = tokenize(train_data);
    
    vector<long double> max_values = find_maximums(tokenized_train_data);
    vector<long double> min_values = find_minimums(tokenized_train_data);

    int correct_classifications = 0;
    for(int i = 0; i < tokenized_train_data.size(); i++) {
        int max_scored_class = -1; 
        long double max_score = MIN;
        for(int j = 0; j < NUMBER_OF_CLASSES; j++) {
            long double score = calc_class_score(tokenized_train_data[i], tokenized_weights[j], max_values, min_values);    
            if (score > max_score){
                max_score = score;
                max_scored_class = j;
            }
        }
        int expected_ans = tokenized_train_data[i][ tokenized_train_data[i].size() - 1 ];
        if(max_scored_class == expected_ans) 
            correct_classifications++;
    }
    float accuracy = (float) correct_classifications / tokenized_train_data.size();
    printf("Accuracy: %0.2f", 100 * accuracy);
    cout << "%\n";
    weights_file.close();
    train_file.close();

    return 0;
}