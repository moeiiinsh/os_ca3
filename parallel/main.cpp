#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

#define NUMBER_OF_THREADS 5
#define NUMBER_OF_CLASSES 4
#define MIN -10000000
#define MAX 10000000

struct min_max_data {
   string file_name;
//    int thread_id;
   vector <long double> minimums;
   vector <long double> maximums;
};

int total_correct_predictions = 0;
pthread_mutex_t critical_area;

struct predictor_data {
    vector < vector<long double> > tokenized_train_data;
    vector < vector<long double> > tokenized_weights;
    vector <long double> minimums;
    vector <long double> maximums;
};

struct min_max_data min_max_data_array[NUMBER_OF_THREADS];
struct predictor_data predictor_data_array[NUMBER_OF_THREADS];


vector< vector<long double> > tokenize(vector<string> data) {
    vector< vector<long double> > result;
    for(int i = 0; i < data.size(); i++) {
        string row = data[i];
        string token;
        stringstream row_stream(row);
        vector<long double> tokenized_row;

        while(getline(row_stream,token, ',')) 
            tokenized_row.push_back(stold(token));

        result.push_back(tokenized_row);
    }
    return result;
}

long double calc_class_score(vector<long double> phone_info, vector<long double> class_weights, vector<long double> maximums, vector<long double> minimums) {
    long double total_score = 0.0;
    for(int i = 0; i < phone_info.size() - 1; i++) {
        long double normalized_value = (phone_info[i] - minimums[i]) / (maximums[i] - minimums[i]);
        total_score += (normalized_value * class_weights[i]);
    }
    long double bias = class_weights[phone_info.size() - 1];
    return total_score + bias;
}

void* predict_class(void* tid) {
    int thread_id = (long)tid;    

    int correct_classifications = 0;
    for(int i = 0; i < predictor_data_array[thread_id].tokenized_train_data.size(); i++) {
        int max_scored_class = -1; 
        long double max_score = MIN;
        for(int j = 0; j < NUMBER_OF_CLASSES; j++) {
            long double score = calc_class_score(predictor_data_array[thread_id].tokenized_train_data[i], 
                                                predictor_data_array[thread_id].tokenized_weights[j], 
                                                predictor_data_array[thread_id].maximums, 
                                                predictor_data_array[thread_id].minimums);    
            if (score > max_score){
                max_score = score;
                max_scored_class = j;
            }
        }
        int expected_ans = predictor_data_array[thread_id].tokenized_train_data[i][ predictor_data_array[thread_id].tokenized_train_data[i].size() - 1 ];
        if(max_scored_class == expected_ans) 
            correct_classifications++;
    }
    pthread_mutex_lock(&critical_area);
    total_correct_predictions += correct_classifications;
    pthread_mutex_unlock(&critical_area);
    pthread_exit(NULL);
}


void* calc_min_and_max(void* tid) {
    int thread_id = (long)tid;
    
    ifstream train_file(min_max_data_array[thread_id].file_name);
    string phone_info_row;
    getline(train_file, phone_info_row);
    vector<string> train_data;

    while(getline(train_file, phone_info_row)) 
        train_data.push_back(phone_info_row);

    vector< vector<long double> > tokenized_train_data = tokenize(train_data);

    for(int i = 0; i < tokenized_train_data[0].size(); i++) {
        min_max_data_array[thread_id].maximums.push_back(MIN);
        min_max_data_array[thread_id].minimums.push_back(MAX);
    }
    
    for(int i = 0; i < tokenized_train_data.size(); i++) {
        vector<long double> row = tokenized_train_data[i];
        for(int j = 0; j < row.size(); j++) {
            if(row[j] > min_max_data_array[thread_id].maximums[j])
                min_max_data_array[thread_id].maximums[j] = row[j];
            if(row[j] < min_max_data_array[thread_id].minimums[j])
                min_max_data_array[thread_id].minimums[j] = row[j];
        }
    }
    predictor_data_array[thread_id].tokenized_train_data = tokenized_train_data; 
    train_file.close();
    pthread_exit(NULL);
}


int main(int argc, char **argv) {
    char *data_dir = argv[1];
    pthread_t threads[NUMBER_OF_THREADS];
    int return_code;
    
    string dataset_dir(data_dir);
    string weights_dir = dataset_dir + "weights.csv";
    
	for(long tid = 0; tid < NUMBER_OF_THREADS; tid++) {
        int num = (int) (tid + 1);
        min_max_data_array[tid].file_name = dataset_dir + "train_" + to_string(num) + ".csv";
		return_code = pthread_create(&threads[tid], NULL, calc_min_and_max, (void*)tid);
		if (return_code) {
			printf("ERROR; return code from pthread_create() is %d\n", return_code);
			exit(-1);
		}
	}
    
    for(long tid = 0; tid < NUMBER_OF_THREADS; tid++) {
        void* status;
        return_code = pthread_join(threads[tid], &status);
        if(return_code) {
			printf("ERROR; return code from pthread_join() is %d\n", return_code);
			exit(-1);
		}
    }

    vector <long double> mins = min_max_data_array[0].minimums;
    vector <long double> maxs = min_max_data_array[0].maximums;
    
    for(int i = 1; i < NUMBER_OF_THREADS; i++) {
        vector<long double> minimums = min_max_data_array[i].minimums;
        vector<long double> maximums = min_max_data_array[i].maximums;
        for(int j = 0; j < minimums.size(); j++) {
            if(maximums[j] > maxs[j])
                maxs[j] = maximums[j];
            if(minimums[j] < mins[j])
                mins[j] = minimums[j];
        }
    }

    ifstream weights_file(weights_dir);
    
    string weights_row;
    getline(weights_file, weights_row);

    vector<string> class_weights;

    while(getline(weights_file, weights_row)) 
        class_weights.push_back(weights_row);

    weights_file.close();

    vector< vector<long double> > tokenized_weights = tokenize(class_weights);    

    for(int i = 0; i < NUMBER_OF_THREADS; i++) {
        predictor_data_array[i].tokenized_weights = tokenized_weights;
        predictor_data_array[i].minimums = mins;
        predictor_data_array[i].maximums = maxs;
    }

    for(long tid = 0; tid < NUMBER_OF_THREADS; tid++) {
        return_code = pthread_create(&threads[tid], NULL, predict_class, (void*)tid);
		if (return_code) {
			printf("ERROR; return code from pthread_create() is %d\n", return_code);
			exit(-1);
		}
	}

    for(long tid = 0; tid < NUMBER_OF_THREADS; tid++) {
        void* status;
        return_code = pthread_join(threads[tid], &status);
        if(return_code) {
			printf("ERROR; return code from pthread_join() is %d\n", return_code);
			exit(-1);
		}
    }
    float accuracy = (float) total_correct_predictions / (NUMBER_OF_THREADS * predictor_data_array[0].tokenized_train_data.size());
    printf("Accuracy: %0.2f", 100 * accuracy);
    cout << "%\n";
    pthread_exit(NULL);
}